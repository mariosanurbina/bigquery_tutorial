--Creación de tabla con structs

CREATE TABLE  nuevodataset.tabla_con_structs (
NUMERO_TRANSACCION STRING NOT NULL,
FECHA_TRANSACCION TIMESTAMP,
VENDEDOR STRING,
CLIENTE STRUCT <NOMBRE STRING, FECHA_NACIMIENTO TIMESTAMP, SEGMENTO STRING >,
TERRITORIO STRUCT <PAIS STRING,ID_ZONA STRING, DESC_ZONA STRING, CUIDAD STRING >,
MONTO_TRANSACCION NUMERIC

);

-- Consultas sobre campos con structs primer nivel

SELECT SUM(totals.visits) AS TOTAL_VISITAS, SUM(totals.hits) AS TOTAL_HITS, device.isMobile , date 
FROM `bigquery-public-data.google_analytics_sample.ga_sessions_20170801` 
GROUP BY date, device.isMobile

-- Consultas sobre campos con structs segundo nivel

SELECT SUM(totals.visits) AS TOTAL_VISITAS, SUM(totals.hits) AS TOTAL_HITS, device.isMobile , date
 , (trafficSource.adwordsClickInfo.adNetworkType) AS LINK
FROM `bigquery-public-data.google_analytics_sample.ga_sessions_20170801` 
GROUP BY date, device.isMobile,LINK


--creación de tabla con ARRAY DE ENTEROS


CREATE TABLE  nuevodataset.tabla_con_array_enteros (
ID INT64 NOT NULL,
ESPACIO STRING,
FECHA  DATE,
TEMPERATURAS ARRAY <INT64>

);

--insertar registros tabla array enteros

INSERT INTO nuevodataset.tabla_con_array_enteros (ID,ESPACIO,FECHA, TEMPERATURAS) 
VALUES (1,'Sala de Maquinas',PARSE_DATE("%d-%m-%Y","01-01-2021"), [ 55,57,59]);

-- ver el registro insertado

SELECT * FROM nuevodataset.tabla_con_array_enteros

--consultas de interes sobre los arrays
--1. Numero de registros que tienen arrays con mas de dos valores

SELECT count(*) FROM nuevodataset.tabla_con_array_enteros WHERE ARRAY_LENGTH(TEMPERATURAS) >=2

--2. Promedio de los valores del array

SELECT ID,ESPACIO,FECHA , (SELECT AVG(B) FROM UNNEST (A.TEMPERATURAS) B) AS PROMEDIO FROM nuevodataset.tabla_con_array_enteros A

--3.Adicionar elementos a un array existente

UPDATE nuevodataset.tabla_con_array_enteros
SET TEMPERATURAS = ARRAY_CONCAT(TEMPERATURAS, [50])
WHERE ID = 1

--creación de tabla con ARRAY DE STRUCTS


CREATE TABLE  nuevodataset.tabla_con_array_enteros_dos (
ID INT64 NOT NULL,
ESPACIO STRING,
TEMPERATURAS ARRAY < STRUCT <MEASURE INT64, AT_TIME TIMESTAMP>>

);

-- Insertar un registro a una tabla que tiene arrays de structs

WITH TEMPERATURAS AS (
  SELECT 1 AS ID,  [STRUCT (57, CURRENT_DATE()),
    STRUCT (59, CURRENT_DATE())] AS TEMPERATURAS
),

ESPACIOS AS (
    SELECT 1 AS ID , 'SALA MAQUINAS' AS ESPACIO
)


INSERT INTO nuevodataset.tabla_con_array_enteros_dos 
SELECT E.ID , E.ESPACIO , T.TEMPERATURAS FROM
ESPACIOS E LEFT JOIN TEMPERATURAS T ON E.ID = T.ID








